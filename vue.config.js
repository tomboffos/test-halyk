const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
      allowedHosts: 'all',
      proxy: {
          '/data/object': {
              target: 'https://halykactiv.web-reactor.tech/',
              secure: false,
              changeOrigin: true
          },
          '/data/objectgk': {
              target: 'https://halykactiv.web-reactor.tech/',
              secure: false,
              changeOrigin: true
          },
          '/data/comtypes': {
              target: 'https://halykactiv.web-reactor.tech/',
              secure: false,
              changeOrigin: true
          },
          '/data/listgk': {
              target: 'https://halykactiv.web-reactor.tech/',
              secure: false,
              changeOrigin: true
          },
      },
  }
})