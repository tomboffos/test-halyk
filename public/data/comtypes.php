<?php
$strdata = file_get_contents('test.json');
if($strdata === false) {
    die("Error read data");
}

$json_all = json_decode( $strdata, true);
if($json_all === null) {
    die("Error decode data");
}
$all_types = array();
$all_types['0'] = array();
$all_types['1'] = array();
$all_types['2'] = array();

foreach ($json_all['locations'] as $object) {
    //if($object['type'] == 1)
    //{
    if(!in_array($object['typeloc'], $all_types[$object['type']]))
    {
        $all_types[$object['type']][] = $object['typeloc'];
    }
    //}
}

print("[");
$fbc = 0;
for ($i=0; $i < 3; $i++) {
    foreach($all_types[$i] as $onetype) {
        if($fbc++ != 0) print(",");
        print("{\"typname\": \"{$onetype}\", \"type\": {$i} }");
    }
}
print("]");

header('Content-Type: application/json; charset=utf-8');
//echo json_encode($all_types);
exit();
