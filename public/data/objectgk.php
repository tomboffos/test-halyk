<?php
$reqID = $_GET['id'];

$strdata = file_get_contents('testgk.json');
if($strdata === false)
{
    die("Error read data");
}

$json_all = json_decode( $strdata, true);
if($json_all === null)
{
    die("Error decode data");
}

foreach ($json_all['objectsgk'] as $object)
{
    if($object['id'] == $reqID)
    {
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($object);
        exit();
    }
}
