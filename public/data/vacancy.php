<?php

$resp = array('message' => '');
if (isset($_POST['name']) && isset($_POST['uploadcv']) && $_POST['uploadcv'] == 'vacancy') {
    $cvName = $_POST['name'];
    $cvPhone = $_POST['phone'];
    $cvMail = $_POST['email'];
    $cvMess = $_POST['message'];
    $cvDate = date('d-m-Y_H:i:s');
    $uploadFileDir = '/var/www/uploadcv/';
    $newTextName = $cvDate.'-'.$cvName.'.txt';
    file_put_contents($uploadFileDir.$newTextName, "Имя: ".$cvName."\n", FILE_APPEND);
    file_put_contents($uploadFileDir.$newTextName, "Телефон: ".$cvPhone."\n", FILE_APPEND);
    file_put_contents($uploadFileDir.$newTextName, "Email: ".$cvMail."\n", FILE_APPEND);
    file_put_contents($uploadFileDir.$newTextName, "Message: ".$cvMess."\n", FILE_APPEND);

    if (isset($_FILES['attachment'])) {
        if ($_FILES['attachment']['error'] === UPLOAD_ERR_OK) {
            $fileTmpPath = $_FILES['attachment']['tmp_name'];
            $fileName = $_FILES['attachment']['name'];
            $fileSize = $_FILES['attachment']['size'];
            $fileType = $_FILES['attachment']['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = $cvDate . '-' . $cvName . '-' . $fileName;

            $allowedfileExtensions = array('txt', 'doc', 'docx', 'pdf');

            if (in_array($fileExtension, $allowedfileExtensions)) {
                $dest_path = $uploadFileDir . $newFileName;

                if (move_uploaded_file($fileTmpPath, $dest_path)) {
                    $resp['message'] = 'Ваше резюме отправлено!';
                } else {
                    $resp['message'] = 'Ошибка сохранения резюме!';
                }
            } else {
                $resp['message'] = 'Поддерживается загрузка только: txt, doc, docx и pdf';
            }
        } else {
            $resp['message'] = 'Ошибка!<br>';
            $resp['message'] .= 'Error message:' . $_FILES['uploadedFile']['error'];
        }
    } else {
        $resp['message'] = 'Ваше сообщение отправлено!';
    }
}

header("Content-Type: application/json");
echo json_encode($resp);
