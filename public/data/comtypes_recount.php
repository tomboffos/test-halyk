<?php
$reqID = $_GET['id'];

$strdata = file_get_contents('test.json');
if($strdata === false)
{
    die("Error read data");
}

$json_all = json_decode( $strdata, true);
if($json_all === null)
{
    die("Error decode data");
}
$all_types = array();

foreach ($json_all['locations'] as $object)
{
    if($object['typr'] == 2)
    {
        if(!in_array($object['typeloc'], $all_types))
        {
            $all_types[] = $object['typeloc'];
        }
    }
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($object);
exit();
