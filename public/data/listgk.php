<?php
$strdata = file_get_contents('testgk.json');
if($strdata === false)
{
    die("Error read data");
}

$json_all = json_decode( $strdata, true);
if($json_all === null)
{
    die("Error decode data");
}

print("[");
$fbc = 0;

foreach ($json_all['objectsgk'] as $object)
{
    if($fbc++ != 0) print(",");
    print("{\"id\": {$object['id']}, \"fullname\": \"{$object['fullname']}\", \"isPage\": 1 }");
}

print(",{\"id\": 10, \"fullname\": \"ЖК Шахристан\", \"isPage\": 0 }");
print(",{\"id\": 16, \"fullname\": \"ЖК Науаи\", \"isPage\": 0 }");
print(",{\"id\": 153, \"fullname\": \"ЖК Esentai City\", \"isPage\": 0 }");

print("]");

header('Content-Type: application/json; charset=utf-8');
exit();
