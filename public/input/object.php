<?php 
if (!$_SERVER["DOCUMENT_ROOT"]) {
    $_SERVER["DOCUMENT_ROOT"] = "/var/www/halykactiv.kz";  
}


define("LANG", "s1");
define("BX_UTF", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_BUFFER_USED", true);

 

class BXObjectCRM {
    private $username = "portal";
    private $password = "y@sffhg55!GK";
    private $url="http://crm.halykactiv.kz/api/portal";
    private $context;
    private $imagesDir;
    private $root;
    
    function __construct() {
        $this->init();
    }
    
    private function init() {
        $this->context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("{$this->username}:{$this->password}")
            )
        ));
        $this->root = $_SERVER["DOCUMENT_ROOT"];
        $this->imagesDir = '/upload/crm/';
        
        
    }
    
    private function get($params, $asJson=false) {
        $out = file_get_contents($this->url . $params , false, $this->context);
        if ($asJson) {
            $out = json_decode($out, true);
        } 
        return $out;
    }
    
    public function response($items) {
        echo json_encode($items);
        
    }
    
    
    
    public function getObject($id) {
        $json = $this->get('/getlocationbyobjectid?id=' . $id, true); 
        $response = [];
        foreach($json['locations'] as $item_){
            $item = [
               'name' => $item_['Name'],
               'section_name' => $item_['Section_Name'],
               'floor' => $item_['Floor'],
               'full_nomer' => $item_['FullNomer'],
               'cell_h' => $item_['CellH'],
               'area_sl' => $item_['AreaSL'],
               'room_count' => $item_['RoomCount'],
               'img' => $this->getImage($item_['File']),
               'address' => $item_['Address'],
               'spotcode' => $item_['SpotCode'],
               'place_name' => $item_['PlaceName'],
               'price' => $item_['Price'],
               'cost' => $item_['Cost'], 
               'cost_format' => number_format($item_['Cost'], 0, ',', ' '), 
            ];
            
            $response[] = $item;
            
            
        }
        
        $this->response($response);
    }
    
    public function getImage($basename) {
        if (!$basename) {
            return '';
        }
        $filepath = $this->buildImagePath($basename); 
        if (file_exists($this->root . $filepath )) {
            return $filepath;
        }
        $file = $this->get('/getfile?path=' . $basename);
        if ($file) {
            file_put_contents($this->root . $filepath, $file );
            return $filepath;
        }
        
        return '';
    }
    
    
    private function buildImagePath($basename) {
        $firstLetter = substr($basename, 0, 1);
        if (!is_dir($this->root . $this->imagesDir .  $firstLetter)) {
            mkdir($this->root . $this->imagesDir . $firstLetter,  0777 );
        }
        return  $this->imagesDir .  $firstLetter . '/' . $basename;
        
    }
    
    
}

$class = new BXObjectCRM();
if (isset($_REQUEST['object_id']) && $_REQUEST['object_id']) {
   $class->getObject($_REQUEST['object_id']); 
}




//print_r($objects);