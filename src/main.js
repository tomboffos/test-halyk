import { createApp } from 'vue';
import App from './App.vue';
import components from '@/components/UI/index';
import router from '@/router/router';
import store from '@/store/index';
import breadcrumbs from 'vue-3-breadcrumbs';
import { Tabs, Tab } from 'vue3-tabs-component';
import YmapPlugin from 'vue-yandex-maps';
import { createI18n, useI18n } from 'vue-i18n'
import {languages} from './i18n'
import { defaultLocale } from './i18n'

const localStoragelang = localStorage.getItem('lang')

const messages = Object.assign(languages)
const i18n = createI18n({
  legacy: false,
  locale: localStoragelang || defaultLocale,
  fallbackLocale: "ru",
  messages
})


const app = createApp(App, {
  setup() {
    const {t} = useI18n()
    return {t}
  }
});

// Components from path /component
components.forEach((component) => {
  app.component(component.name, component);
});

// Global components registration
app.component('tabs', Tabs);
app.component('tab', Tab);

const settings = {
  apiKey: '6c5b0d22-08a5-434b-a769-58b060aa44b5',
  lang: 'ru_RU',
  coordorder: 'latlong',
  enterprise: false,
  version: '2.1',
};

app.use(YmapPlugin, settings);

app
  .use(router)
  .use(store)
  .use(breadcrumbs, {
    includeComponent: true, // {boolean} [includeComponent=false] - Include global breadcrumbs component or not
  })
  .use(i18n)
  .mount('#app');
