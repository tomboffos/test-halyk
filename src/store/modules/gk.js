import axios from 'axios';

export default {
  actions: {
    fetchInfoGk({ commit }, id) {
      axios.get(`/data/objectgk?id=${id}`).then((data) => {
        commit('commitInfoToState', data.data);
      });
    },
    fetchAllGk({ commit }) {
      axios('/data/listgk').then((data) => {
        commit('commitAllGkToState', data.data);
      });
    },
  },
  mutations: {
    commitInfoToState(state, payload) {
      state.info = payload;
    },
    commitAllGkToState(state, payload) {
      state.gk = payload;
    },
  },
  state: {
    info: [],
    gk: [],
  },
  getters: {
    gkinfo(state) {
      return state.info;
    },
    allgk(state) {
      return state.gk;
    },
  },
};
