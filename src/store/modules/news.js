import axios from 'axios';

export default {
  actions: {
    getAllNews({ commit }) {
      axios.get('./data/news.json').then((res) => {
        commit('commitAllNews', res.data);
      });
    },
  },
  mutations: {
    commitAllNews(state, payload) {
      state.news = payload;
    },
  },
  state: {
    news: [],
  },
  getters: {
    getNewsForMainPage(state) {
      return state.news.slice(0, 4);
    },
    getOtherNewsForNewsPage(state) {
      let newArr = state.news.reverse()
      return newArr.slice(0, 4);
    },
    getNews(state) {
      return state.news;
    },

    getNewsFromId: (state) => {
      return state.news;
    },
  },
};
