import axios from 'axios';
import _ from 'lodash';

export default {
  actions: {
    fetchAllTypesLoc({ commit }) {
      axios
        .get(`/data/comtypes`)
        .then((res) => {
          commit('getAllTypesLoc', res.data);
        })
        .catch((err) => console.log(err));
    },
    fetchObjectById({ commit }, id) {
      axios
        .get(`/data/object?id=${id}`)
        .then((res) => commit('getObjectItem', res.data))
        .catch((err) => console.log(err));
    },
    ApiGetAllObjects({ commit }, { payload, filter }) {
      axios
        .get('/data/test.json')
        .then((res) => {
          let objects = res.data.locations.filter((el) => el.type === payload);

          commit('commitAllObjects', objects);

          if (Object.keys(filter).length !== 0) {
            filter.square
              ? commit('updateStateFilters', {
                  square: { min: +filter.square },
                })
              : '';
            filter.rooms
              ? commit('updateStateFilters', { rooms: [filter.rooms] })
              : '';
            filter.gkplace
              ? commit('updateStateFilters', { gkplace: [filter.gkplace] })
              : '';

            filter.cost
              ? commit('updateStateFilters', {
                  cost: { min: parseInt(filter.cost) },
                })
              : '';

            filter.city && filter.city != '-1'
              ? commit('updateStateFilters', { city: filter.city })
              : '';

            filter.typeloc && filter.typeloc != '-1'
              ? commit('updateStateFilters', { typeloc: filter.typeloc })
              : '';
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },

    fetchObjectsGk({ commit }, payload) {
      axios
        .get('/data/test.json')
        .then((res) => {
          let objects = res.data.locations.filter(
            (el) => el.gkplace === +payload
          );

          commit('commitAllObjects', objects);
        })
        .catch((err) => {
          console.log(err);
        });
    },

    updateFilters({ commit }, options) {
      commit('updateStateFilters', options);
    },

    updateSort({ commit }, options) {
      commit('updateStateSort', options);
    },
  },
  mutations: {
    getObjectItem(state, payload) {
      state.object = payload;
    },
    updateStateFilters(state, payload) {
      if (payload == 'clear') {
        state.filters.rooms.length = 0;
      } else if (payload == 'cleargk') {
        state.filters.gkplace.length = 0;
      } else {
        state.filters = _.merge(state.filters, payload);
      }
    },
    updateStateSort(state, payload) {
      state.sort = payload;
    },

    getAllTypesLoc(state, payload) {
      state.typesloc = payload;
    },

    commitAllObjects(state, payload) {
      state.objects = payload;
      let floorArr = payload.map((el) => +el.floor);
      state.filters.stage = {
        minRange: _.min(floorArr),
        min: _.min(floorArr),
        max: _.max(floorArr),
        maxRange: _.max(floorArr),
      };

      let sqareArr = payload.map((el) => +el.square);
      state.filters.square = {
        minRange: _.min(sqareArr) > 0 ? _.min(sqareArr) - 1 : 0,
        min: _.min(sqareArr) > 0 ? _.min(sqareArr) - 1 : 0,
        max: _.max(sqareArr),
        maxRange: _.max(sqareArr),
      };

      let priceArr = payload.map((el) => +el.price);
      state.filters.price = {
        minRange: _.min(priceArr),
        min: _.min(priceArr),
        max: _.max(priceArr),
        maxRange: _.max(priceArr),
      };

      let costArr = payload.map((el) => +el.cost);
      state.filters.cost = {
        minRange: _.min(costArr),
        min: _.min(costArr),
        max: _.max(costArr),
        maxRange: _.max(costArr),
      };

      state.filters.city = -1;
      state.filters.typeloc = -1;

      state.filters.rooms = [];
      state.filters.gkplace = [-1];
    },
  },
  state: {
    objects: [],
    object: {},
    sort: {
      value: 'typeloc',
      az: 1,
    },
    typesloc: [],
    filters: {
      city: -1,
      typeloc: -1,
      rooms: [],
      gkplace: [-1],
      type: 0,
      stage: {
        minRange: 0,
        maxRange: 1,
        min: 0,
        max: 1,
      },
      square: {
        minRange: 0,
        maxRange: 1,
        min: 0,
        max: 1,
      },
      price: {
        minRange: 0,
        maxRange: 1,
        min: 0,
        max: 1,
      },
      cost: {
        minRange: 0,
        maxRange: 1,
        min: 0,
        max: 1,
      },
    },
  },
  getters: {
    rooms: (state) => {
      let tmp = state.objects

        .filter(
          (el) =>
            el.floor >= state.filters.stage.min &&
            el.floor <= state.filters.stage.max
        )
        .filter(
          (el) =>
            el.square >= state.filters.square.min &&
            el.square <= state.filters.square.max
        )
        .filter(
          (el) =>
            el.price >= state.filters.price.min &&
            el.price <= state.filters.price.max
        )
        .filter(
          (el) =>
            el.cost >= state.filters.cost.min &&
            el.cost <= state.filters.cost.max
        );

      if (state.filters.city !== -1) {
        tmp = tmp.filter((el) => el.city.includes(state.filters.city));
      }

      if (state.filters.rooms.length != 0) {
        let intRoom = state.filters.rooms.map((el) => parseInt(el));
        tmp = tmp.filter((el) => intRoom.includes(el.rooms));
      }

      if (state.filters.gkplace.length !== 0) {
        let intGk = state.filters.gkplace.map((el) => parseInt(el));
        if (
          state.filters.gkplace.indexOf(-1) === -1 &&
          state.filters.gkplace.indexOf('-1') === -1
        ) {
          tmp = tmp.filter((el) => intGk.includes(el.gkplace));
        }
      }

      if (state.filters.typeloc !== -1) {
        tmp = tmp.filter((el) => el.typeloc == state.filters.typeloc);
      }

      if (state.sort.az) {
        tmp = _.sortBy(tmp, state.sort.value);
      } else {
        tmp = _.sortBy(tmp, state.sort.value).reverse();
      }

      return tmp;
    },

    roomsActivePage: (state, getters) => (first, count) => {
      return getters.rooms.slice(first, count);
    },

    total: (state, getters) => Math.ceil(getters.rooms.length / 15),

    totalObjects: (setter, getters) => {
      if (getters.rooms.length !== 0) {
        return getters.rooms.length;
      } else {
        return getters.commercialRooms.length;
      }
    },

    totalGkObjects: (state, getters) => {
      return getters.roomsGk.length;
    },

    city: (state) => {
      let cityArr = state.objects.map((el) => el.city);
      cityArr = Array.from(new Set(cityArr));

      return cityArr.map((el, index) => ({
        name: el,
        value: index,
      }));
    },
    typeloc: (state) => {
      return state.typesloc.map((el, index) => ({
        name: el.typname,
        value: index,
        type: el.type,
      }));
    },

    activeTypeloc: (state) => {
      return state.filters.typeloc;
    },

    maxCostValue: (state) => {
      let res = _.maxBy(state.objects, 'cost').cost;
      return res;
    },

    filters: (state) => {
      return state.filters;
    },

    commercialRooms: (state) => {
      let tmp = state.objects
        .filter((el) => el.type === 1)
        .filter(
          (el) =>
            el.floor >= state.filters.stage.min &&
            el.floor <= state.filters.stage.max
        )
        .filter(
          (el) =>
            el.square >= state.filters.square.min &&
            el.square <= state.filters.square.max
        )
        .filter(
          (el) =>
            el.price >= state.filters.price.min &&
            el.price <= state.filters.price.max
        )
        .filter(
          (el) =>
            el.cost >= state.filters.cost.min &&
            el.cost <= state.filters.cost.max
        )
        .filter((el) => {
          if (state.filters.typeloc === -1) {
            return el;
          } else {
            return el.typeloc === state.filters.typeloc;
          }
        })
        .filter((el) => {
          if (state.filters.city === -1) {
            return el;
          } else {
            return el.city.includes(state.filters.city);
          }
        });

      if (state.sort.az) {
        tmp = _.sortBy(tmp, state.sort.value);
      } else {
        tmp = _.sortBy(tmp, state.sort.value).reverse();
      }

      return tmp;
    },

    commercialRoomsActivePage: (state, getters) => (from, to) => {
      return getters.commercialRooms.slice(from, to);
    },

    totalCommercial: (state, getters) => {
      return Math.ceil(getters.commercialRooms.length / 15);
    },

    object: (state) => {
      return state.object;
    },

    roomsGk: (state) => {
      let tmp = state.objects
        .filter(
          (el) =>
            el.floor >= state.filters.stage.min &&
            el.floor <= state.filters.stage.max
        )
        .filter(
          (el) =>
            el.square >= state.filters.square.min &&
            el.square <= state.filters.square.max
        )
        .filter(
          (el) =>
            el.price >= state.filters.price.min &&
            el.price <= state.filters.price.max
        )
        .filter(
          (el) =>
            el.cost >= state.filters.cost.min &&
            el.cost <= state.filters.cost.max
        )
        .filter((el) => {
          if (state.filters.city === -1) {
            return el;
          } else {
            return el.city.includes(state.filters.city);
          }
        })
        .filter((el) => {
          if (state.filters.typeloc === -1) {
            return el;
          } else {
            return el.typeloc === state.filters.typeloc;
          }
        })

        .filter((el) => {
          if (state.filters.rooms.length != 0) {
            return state.filters.rooms.includes(el.rooms);
          } else {
            return el;
          }
        });

      if (state.sort.az) {
        tmp = _.sortBy(tmp, state.sort.value);
      } else {
        tmp = _.sortBy(tmp, state.sort.value).reverse();
      }

      return tmp;
    },

    gkRoomsActivePage: (state, getters) => (from, to) => {
      return getters.roomsGk.slice(from, to);
    },

    totalGk: (state, getters) => {
      return Math.ceil(getters.roomsGk.length / 15);
    },
  },
};
