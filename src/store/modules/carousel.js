import axios from 'axios';

export default {
  actions: {
    getCarouselItems({ commit }) {
      axios.get('./data/carouselData.json').then((data) => {
        commit('commitItemsToState', data.data);
      });
    },
  },
  mutations: {
    commitItemsToState(state, payload) {
      state.objectList = payload;
    },
  },
  state: {
    objectList: [],
  },
  getters: {
    getItems(state) {
      return state.objectList;
    },
  },
};
