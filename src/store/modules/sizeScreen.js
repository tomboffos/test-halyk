export default {
  actions: {
    getSize({ commit }, payload) {
      commit('updateSizeScreen', payload);
    },
  },
  mutations: {
    updateSizeScreen(state, payload) {
      state.size = payload;
    },
  },
  state: {
    size: 'desktop',
  },
  getters: {
    sizeScreen(state) {
      return state.size;
    },
  },
};
