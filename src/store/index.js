import { createStore } from 'vuex';
import objects from '@/store/modules/objects';
import carousel from '@/store/modules/carousel';
import news from '@/store/modules/news';
import gk from '@/store/modules/gk';
import size from '@/store/modules/sizeScreen';

const store = createStore({
  modules: {
    objects,
    carousel,
    news,
    gk,
    size,
  },
});

export default store;
