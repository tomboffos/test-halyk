import ButtonGreen from '@/components/UI/ButtonGreen';
import ButtonOrange from '@/components/UI/ButtonOrange';
import FooterComponent from '@/components/FooterComponent';
import HeaderComponent from '@/components/HeaderComponent';
import ButtonWhite from '@/components/UI/ButtonWhite';
import InputField from '@/components/UI/InputField';
import TextareaField from '@/components/UI/TextareaField';
import TabsComponent from '@/components/UI/TabsComponent';
import SelectField from '@/components/UI/SelectField';
import CarouselComponent from '@/components/CarouselComponent';
import CarouselItem from '@/components/UI/CarouselItem';
import NewsComponent from '@/components/NewsComponent';
import MoreNewsComponent from '@/components/MoreNewsComponent';
import SideBarComponent from '@/components/SideBarComponent';
import MultiRangeSlider from '@/components/UI/MultiRangeSlider';
import Pagination from '@/components/UI/Pagination';
import SortComponent from '@/components/SortComponent';
import SliderComponent from '@/components/SliderComponent';
import Modal from '@/components/Modal';
import ModalFirst from '@/components/ModalFirst';
import CalculatorComponent from '@/components/CalculatorComponent';
import ConsultComponent from '@/components/ConsultComponent';

export default [
  ButtonGreen,
  ButtonOrange,
  ButtonWhite,
  FooterComponent,
  HeaderComponent,
  InputField,
  TextareaField,
  TabsComponent,
  SelectField,
  CarouselComponent,
  CarouselItem,
  NewsComponent,
  ModalFirst,
  MoreNewsComponent,
  SideBarComponent,
  MultiRangeSlider,
  Pagination,
  SortComponent,
  SliderComponent,
  Modal,
  CalculatorComponent,
  ConsultComponent,
];
