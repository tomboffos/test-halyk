import MainPage from '@/pages/MainPage';
import ObjectsPage from '@/pages/ObjectsPrivatPage';
import AboutPage from '@/pages/AboutPage';
import ObjectsCommercialPage from '@/pages/ObjectsCommercialPage';
import ContactsPage from '@/pages/ContactsPage';
import { createRouter, createWebHistory } from 'vue-router';
import NewsPage from '@/pages/NewsPage';
import NewsItemPage from '@/pages/NewsItemPage';
import ObjectItemPage from '@/pages/ObjectItemPage';
import ObjectsGkPage from '@/pages/ObjectsGkPage';
import ObjectsSecondPage from '@/pages/ObjectsSecondPage';
import VacancyPage from '@/pages/VacancyPage';

const routes = [
  {
    path: '/',
    component: MainPage,
    name: 'main',
    meta: {
      breadcrumb: 'Главная', // Can be just a string
    },
  },
  {
    path: '/privat',
    component: ObjectsPage,
    name: 'privat',
    meta: {
      breadcrumb: {
        label: 'Жилая недвижимость',
        link: '/privat', // custom link
      },
    },
  },
  {
    path: '/objects/:id',
    component: ObjectItemPage,
    name: 'objects',
    meta: {
      breadcrumb: {
        label: 'Объекты',
        // custom link
      },
    },
  },
  {
    path: '/gk/:id',
    component: ObjectsGkPage,
    name: 'gk',
    meta: {
      breadcrumb: {
        label: 'Жилой комплекс',
        // custom link
      },
    },
  },
  {
    path: '/second',
    component: ObjectsSecondPage,
    name: 'second',
    meta: {
      breadcrumb: {
        label: 'Вторичная недвижимость',
        // custom link
      },
    },
  },
  {
    path: '/commercial',
    component: ObjectsCommercialPage,
    name: 'commercial',
    meta: {
      breadcrumb: {
        label: 'Коммерческая недвижимость',
        link: '/commercial', // custom link
      },
    },
  },
  {
    path: '/contacts',
    component: ContactsPage,
    name: 'contacts',
    meta: {
      breadcrumb: {
        label: 'Контакты',
        link: '/contacts', // custom link
      },
    },
  },
  {
    path: '/about',
    component: AboutPage,
    name: 'about',
    meta: {
      breadcrumb: {
        label: 'О нас',
        link: '/about', // custom link
      },
    },
  },
  {
    path: '/news',
    component: NewsPage,
    name: 'news',
    meta: {
      breadcrumb: {
        label: 'Новости',
        link: '/news', // custom link
      },
    },
  },
  {
    path: '/news/:id',
    component: NewsItemPage,
    name: 'news-item',
  },
  {
    path: '/vacancy',
    component: VacancyPage,
    name: 'vacancy',
    meta: {
      breadcrumb: {
        label: 'Вакансии',
        link: '/vacancy', // custom link
      },
    },
  },
];

const router = createRouter({
  routes,
  history: createWebHistory(process.env.BASE_URL),
});

export default router;
